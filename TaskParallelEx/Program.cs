﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace TaskParallelEx
{
    class Program
    {
        private static long BigTask()
        {
            long total = 0;
            for (int i = 1; i < 100000000; i++)
            {
                total += i;
            }
            return total;
        }

        static void Main(string[] args)
        {
            DateTime startDateTime = DateTime.Now;
            Console.WriteLine(@"for loop");
            for (int i = 0; i < 10; i++)
            {
                long total = BigTask();
                Console.WriteLine("{0} - {1}", i, total);
            }
            DateTime endDateTime = DateTime.Now;
            TimeSpan span = endDateTime - startDateTime;
            Console.WriteLine("Time taken in miliseconds {0}", (int)span.TotalMilliseconds);

            // TPL
            startDateTime = DateTime.Now;
            Console.WriteLine("parallel");
            Parallel.For(0, 10, i => {
                long total = BigTask();
                Console.WriteLine("{0} - {1}", i, total);
            });
            endDateTime = DateTime.Now;
            span = endDateTime - startDateTime;
            Console.WriteLine("Time taken in miliseconds {0}", (int)span.TotalMilliseconds);

            Console.ReadLine();
        }
    }
}
